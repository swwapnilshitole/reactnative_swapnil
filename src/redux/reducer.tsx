import {SIGN_IN_USER_DETAILS} from './constants';

interface Action {
  type: string;
  data: boolean;
}

const initialState = {
  userSignIn: false,
};

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case SIGN_IN_USER_DETAILS:
      return {...state, userSignIn: action.data};    
    default:
      return state;
  }
};
