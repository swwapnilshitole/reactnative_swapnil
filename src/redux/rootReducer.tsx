import {combineReducers, Reducer} from 'redux';
import {reducer} from './reducer';
import { checkSignInUser} from './userReducer';

export interface RootState {
  cart: any; 
  checkSignIn: any; 
}

interface Action {
  type: string;
  data: boolean;
}



const rootReducer: Reducer<RootState, Action> = combineReducers({
  cart: reducer,
  checkSignIn: checkSignInUser,
});

export default rootReducer;
